
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
import pandas.io.formats.excel as excel_format
from xlsxwriter.utility import xl_rowcol_to_cell, xl_col_to_name, xl_range


# In[3]:


excel = pd.ExcelWriter('Отчет.xlsx',  engine='xlsxwriter')
excel_format.header_style = None
#pd.formats.format.header_style = None

# Создание базовых стилей
workbook  = excel.book
font_fmt = workbook.add_format({'font_name': 'Verdana', 'font_size': 10})
border_fmt = workbook.add_format({'border': 1})
time_fmt = workbook.add_format({'font_name': 'Verdana', 'font_size': 10, 'num_format':'[hh]:mm:ss'})


# In[4]:


# Загружаем данные
df = pd.read_csv('toggl.csv', sep=',', parse_dates=[['Start date','Start time'], ['End date', 'End time']], 
                 infer_datetime_format= True)
df.head(1)


# In[5]:


# Дропаем ненужные поля
df.drop(['User','Email', 'Billable', 'Tags', 'Amount ()', 'Task'], axis=1, inplace=True)
# Заполняем пустые поля, конвертим в нужным тип столбцы, переименовываем
df.fillna('None', inplace=True)
df['Duration'] = pd.to_timedelta(df['Duration'])
df.rename(columns={'Start date_Start time': 'Start', 'End date_End time': 'End'}, inplace=True)
df.columns = ['Начало', 'Конец', 'Клиент', 'Проект', 'Описание работы', 'Трудозатраты']
df.set_index('Начало',inplace=True)
df.head(1)


# In[6]:


# Строим общую сводную таблицу по всем трудозатратам в разрезе всего времени
pt_summary = pd.pivot_table(df,index=['Клиент','Проект',], values=['Трудозатраты'], aggfunc=np.sum)
pt_summary.to_excel(excel,'Общие трудозатраты')
pt_summary


# In[7]:


# Красиво оформляем
worksheet = excel.sheets['Общие трудозатраты']
row_num, _ = pt_summary.shape
table_cells = xl_range (0,0,row_num,2)
bar_cells = xl_range (1,2,row_num,2)
worksheet.set_column('A:A', 14, font_fmt)
worksheet.set_column('B:B', 41, font_fmt)
worksheet.set_column('C:C', 20, time_fmt)
worksheet.conditional_format(table_cells, {'type': 'cell', 'criteria': '!=', 'value': '1', 'format': border_fmt})
worksheet.conditional_format(bar_cells, {'type': 'data_bar','bar_color': '#37115A'})


# In[8]:


pt_daysummary = pd.pivot_table(df,index=['Клиент', 'Проект'], values=['Трудозатраты'], columns=[df.index.date], 
                               aggfunc=np.sum, fill_value=0)
pt_daysummary.to_excel(excel,'Трудозатраты по дням')
pt_daysummary


# In[9]:


# Красиво оформляем
worksheet = excel.sheets['Трудозатраты по дням']
row_num, column_num = pt_daysummary.shape
row_num +=2
column_num +=1

end_column = xl_col_to_name(column_num)
table_columns = "A:{}".format(end_column)
data_columns = "C:{}".format(end_column)

table_cells = xl_range (0,0,row_num, column_num)
#bar_cells = xl_range (1,2,row_num,2)

worksheet.set_column('A:A', 14, font_fmt)
worksheet.set_column('B:B', 41, font_fmt)
worksheet.set_column(data_columns, 20, time_fmt)
worksheet.conditional_format(table_cells, {'type': 'cell', 'criteria': '!=', 'value': '-1', 'format': border_fmt})
for col_num in range(2, column_num+1):
    cell_range = xl_range(3,col_num,row_num,col_num)
    worksheet.conditional_format(cell_range, {'type': 'data_bar','bar_color': '#37115A'})
end_column


# In[10]:


df.to_excel(excel,'Подробно')


# In[11]:


# Красиво оформляем
worksheet = excel.sheets['Подробно']
row_num, column_num = df.shape

table_cells = xl_range (0,0,row_num, column_num)
bar_cells = xl_range (1,5,row_num,5)

worksheet.set_column('A:B', 18, font_fmt)
worksheet.set_column('C:C', 9, font_fmt)
worksheet.set_column('C:C', 9, font_fmt)
worksheet.set_column('D:D', 38, font_fmt)
worksheet.set_column('E:E', 60, font_fmt)
worksheet.set_column('F:F', 20, time_fmt)

worksheet.conditional_format(table_cells, {'type': 'cell', 'criteria': '!=', 'value': '-1', 'format': border_fmt})
worksheet.conditional_format(bar_cells, {'type': 'data_bar','bar_color': '#37115A'})


# In[12]:


excel.save()


# In[13]:


df.index - df['Конец'].shift()


# In[14]:


# Строим общую сводную таблицу по всем трудозатратам в разрезе рабочего времени
df_work = df.between_time('10:00', '19:00')
pt_work_summary = pd.pivot_table(df_work,index=['Проект'], values=['Трудозатраты'], aggfunc=np.sum)
pt_work_summary


# In[15]:


# Строим общую сводную таблицу по всем трудозатратам в разрезе нерабочего времени
df_not_work = df.iloc[df.index.indexer_between_time('19:00', '10:00',include_start=False, include_end=False)]
pt_not_work_summary = pd.pivot_table(df_not_work,index=['Проект'], values=['Трудозатраты'], aggfunc=np.sum)
pt_not_work_summary

